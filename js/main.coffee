app = angular.module 'app', []

app.controller 'TreeCtrl', ['$scope', 'DataService', ($scope, DataService) ->

    if localStorage.getItem 'objects'
        $scope.objects = JSON.parse(localStorage.getItem 'objects')
    else
        DataService.fetch().then (data) ->
            $scope.objects = data.elements
            return

    this.addChild = (child) ->
        child.children.push {"name": "New element", "children": []}
        return
    this.removeChildren = (child) ->
        child.children = []
        return
    $scope.save = (objects) ->
        DataService.save(objects)
        $scope.msg = 'State saved'
        return
    $scope.clear = ->
        DataService.clear()
        $scope.msg = 'State cleared, refresh to load original data'
        return
    return
]
