app = angular.module 'app'

app.directive 'tree', ->
    restrict: 'E'
    replace: true
    scope:
        tree: '='
    templateUrl: 'partials/treeObject.html'

app.directive 'child', ($compile) ->
    restrict: 'E'
    replace: true
    require: '^ngController'
    scope: true
    templateUrl: 'partials/treeChild.html'
    link: (scope, element, attrs, parentCtrl) ->
        tree = '<tree tree="child.children"></tree>'
        if angular.isArray(scope.child.children)
            $compile(tree)(scope, (cloned, scope) ->
                element.append cloned
                return
            )
        scope.addChild = (data) ->
            parentCtrl.addChild(data)
            return
        scope.removeChildren = (child) ->
            parentCtrl.removeChildren(child)
            return
        return
