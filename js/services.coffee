app = angular.module 'app'

app.service 'DataService', ($q, $timeout, $http) ->
    this.fetch = ->
        deferred = $q.defer()
        $timeout ->
            $http.get('data.json').success (result) ->
                deferred.resolve(result)
                return
        , 30
        return deferred.promise
    this.save = (data) ->
        localStorage.setItem('objects', JSON.stringify(data))
        return
    this.clear = ->
        localStorage.clear()
        return
    return
