// Generated by CoffeeScript 1.7.1
(function() {
  describe("DataService", function() {
    var data, scope, service;
    data = null;
    scope = null;
    service = null;
    beforeEach(module("app"));
    beforeEach(inject(function($rootScope, $q, DataService) {
      var deferred;
      deferred = $q.defer();
      scope = $rootScope.$new();
      service = DataService;
      spyOn(service, 'fetch').andReturn(deferred.promise);
      spyOn(service, 'save').andCallThrough();
      spyOn(service, 'clear').andCallThrough();
    }));
    it("should fetch stuff", function() {
      service.fetch();
      expect(service.fetch).toHaveBeenCalled();
    });
    it("should save the state to localStorage", function() {
      service.save('mock data');
      expect(service.save).toHaveBeenCalled();
      expect(localStorage.getItem('objects')).toBeDefined();
    });
    it("should clear the state", function() {
      service.clear();
      expect(service.clear).toHaveBeenCalled();
      expect(localStorage.getItem('objects')).toBeNull();
    });
    afterEach(function() {
      localStorage.clear();
    });
  });

}).call(this);
