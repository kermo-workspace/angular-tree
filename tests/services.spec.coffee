describe "DataService", ->

    data = null
    scope = null
    service = null

    beforeEach module "app"
    beforeEach inject ($rootScope, $q, DataService) ->
        deferred = $q.defer()
        scope = $rootScope.$new()
        service = DataService
        spyOn(service, 'fetch').andReturn(deferred.promise)
        spyOn(service, 'save').andCallThrough()
        spyOn(service, 'clear').andCallThrough()
        return

    it "should fetch stuff", ->
        service.fetch()
        expect(service.fetch).toHaveBeenCalled()
        return

    it "should save the state to localStorage", ->
        service.save('mock data')
        expect(service.save).toHaveBeenCalled()
        expect(localStorage.getItem('objects')).toBeDefined()
        return

    it "should clear the state", ->
        service.clear()
        expect(service.clear).toHaveBeenCalled()
        expect(localStorage.getItem('objects')).toBeNull()
        return

    afterEach ->
        localStorage.clear()
        return

    return
