# Mock data
elements = [
        {
            "name": "Parent",
            "children": [
                {
                    "name": "Child",
                    "children": [
                        {
                            "name": "Grandchild",
                            "children": []
                        }
                    ]
                },
                {
                    "name": "Child",
                    "children": []
                }
            ]
        },
        {
            "name": "Parent",
            "children": [
                {
                    "name": "Child",
                    "children": []
                },
                {
                    "name": "Child",
                    "children": []
                }
            ]
        }
    ]

describe "TreeCtrl", ->

    scope = null
    service = null
    ctrl = null
    beforeEach module "app"
    beforeEach inject ($rootScope, $controller, $q, DataService) ->

        scope = $rootScope.$new()
        service = DataService
        deferred = $q.defer()

        # Init the service
        spyOn(service, 'fetch').andReturn(deferred.promise)

        # Init the controller
        ctrl = $controller "TreeCtrl", {$scope: scope, DataService: service}
        spyOn(ctrl, 'addChild').andCallThrough()
        spyOn(ctrl, 'removeChildren').andCallThrough()

        # Add mock data
        scope.objects = elements
        return

    it "should populate scope with objects", ->
        expect(service.fetch).toHaveBeenCalled()
        expect(scope.objects.length).toBe(2)
        return

    it "should add a child", ->
        ctrl.addChild(scope.objects[0])
        expect(ctrl.addChild).toHaveBeenCalled()
        expect(scope.objects[0].children.length).toBe(3)
        return

    it "should remove children", ->
        ctrl.removeChildren(scope.objects[0])
        expect(ctrl.removeChildren).toHaveBeenCalled()
        expect(scope.objects[0].children.length).toBe(0)

    return
